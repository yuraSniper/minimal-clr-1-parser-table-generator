﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Minimal_LR1_parser_generator
{
	public partial class Form2 : Form
	{
		Stack<string> stack = new Stack<string>();
		Stack<int> stateStack = new Stack<int>();
		int caret = 0;
		string[] input;

		public Form2()
		{
			InitializeComponent();
		}

		private void ParserStep()
		{
			int state = stateStack.Peek();
			string nextToken = input[caret];
			int nextTokenIndex = ParserGen.symbols.FindIndex(x => x.lexeme == nextToken);
			
			if (nextTokenIndex < 0 || nextTokenIndex > ParserGen.actionTable.Count)
				return;
			
			switch (ParserGen.actionTable[nextTokenIndex][state].action)
			{
				case ActionType.SRConflict:
				case ActionType.Shift:
					stack.Push(nextToken);
					stateStack.Push(ParserGen.gotoTable[nextTokenIndex][state]);
					caret++;
					break;

				case ActionType.Accept:
					caret++;
					break;

				case ActionType.Reduce:
					for (int i = 0; i < ParserGen.actionTable[nextTokenIndex][state].reduceByRule.rule.Count; i++)
					{
						stack.Pop();
						stateStack.Pop();
					}

					nextTokenIndex = ParserGen.actionTable[nextTokenIndex][state].reduceByRule.result.id;
					stack.Push(ParserGen.symbols[nextTokenIndex].ToString());
					state = stateStack.Peek();
					stateStack.Push(ParserGen.gotoTable[nextTokenIndex][state]);

					break;

				case ActionType.Error:
					MessageBox.Show("Some syntax error occurred");
					break;

				case ActionType.RRConflict:
					MessageBox.Show("Reduce-reduce conflict in grammar");
					break;
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			stack.Clear();
			stateStack.Clear();
			caret = 0;

			stateStack.Push(0);

			textBox3.Text = "0";

			input = (textBox1.Text + " Eof").Trim(' ').Split(' ');

			button2.Enabled = true;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			if (caret < input.Length)
			{
				ParserStep();
				textBox2.Text = stack.Aggregate("", (a, b) => b + "|" + a);
				textBox3.Text = stateStack.Select(x => x.ToString()).Aggregate("", (a, b) => b + "|" + a);
			}
			else
				button2.Enabled = false;
		}
	}
}
