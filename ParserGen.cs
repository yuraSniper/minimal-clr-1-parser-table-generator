﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Minimal_LR1_parser_generator
{ 
	public class Item
	{
		public Rule rule;
		public int dot;
		public HashSet<Symbol> lookahead;

		public Item(Rule rule, int dot, HashSet<Symbol> lookahead)
		{
			this.rule = rule;
			this.dot = dot;
			this.lookahead = lookahead;
		}

		public Item Clone()
		{
			return new Item(rule, dot, new HashSet<Symbol>(lookahead));
		}

		public override string ToString()
		{
			return rule.ToString(dot, true) + "     " + lookahead.Aggregate("", (s, x) => s + " " + x.lexeme);
		}

		public bool Similar(Item other)
		{
			return rule == other.rule && dot == other.dot;
		}

		public bool Equals(Item other)
		{
			return Similar(other) && lookahead.SetEquals(other.lookahead);
		}

		public bool DoesLookaheadIntersect(Item other)
		{
			return lookahead.Intersect(other.lookahead).Count() != 0;
		}
	}

	public class Rule
	{
		public Symbol result;
		public List<Symbol> rule;
		public int id;

		public override string ToString()
		{
			return ToString(-1, true);
		}

		public string ToString(int dot)
		{
			return ToString(dot, false);
		}

		public string ToString(int dot, bool showResult)
		{
			string res = "";

			if (showResult)
				res = result.lexeme + " -> ";

			if (dot != -1)
			{
				res += rule.GetRange(0, dot).Aggregate("", (s, x) => s + " " + x.lexeme);
				res += " | ";
			}
			else
				dot = 0;

			res += rule.GetRange(dot, rule.Count - dot).Aggregate("", (s, x) => s + " " + x.lexeme);

			return res;
		}
	}

	public class State
	{
		public List<Item> items;
		public HashSet<Symbol> nextSymbols;
		public int id;

		public new string[] ToString()
		{
			string[] itemStrings = new string[items.Count];

			for (int i = 0; i < items.Count; i++)
				itemStrings[i] = items[i].ToString();

			return itemStrings;
		}

		public bool Similar(State other)
		{
			if (items.Count != other.items.Count)
				return false;

			foreach (var item in items)
				if (other.items.All(x => !x.Similar(item))) //If all are not similar
					return false;

			return true;
		}

		public bool Equals(State other)
		{
			if (items.Count != other.items.Count)
				return false;

			foreach (var item in items)
				if (other.items.All(x => !x.Equals(item))) //If no one doesn't equal
					return false;

			return true;
		}

		public State Merge(State other)
		{
			State newState = new State();
			newState.id = id;
			newState.nextSymbols = nextSymbols;
			newState.items = new List<Item>();

			foreach (var item in items)
			{
				int index = other.items.FindIndex(x => x.Similar(item));
				var newLookahead = new HashSet<Symbol>(other.items[index].lookahead.Union(item.lookahead));
				newState.items.Add(new Item(item.rule, item.dot, newLookahead));
			}

			return newState;
		}

		public bool HasRRConflict()
		{
			var reducingItems = GetReducingItems();

			foreach (var item in reducingItems)
			{
				if (reducingItems.Where(x => !x.Equals(item)).Any(x => x.DoesLookaheadIntersect(item)))
					return true;
			}

			return false;
		}

		public List<Item> GetReducingItems()
		{
			return items.Where(x => x.dot == x.rule.rule.Count).ToList();
		}
	}

	public enum ActionType
	{
		Error,
		Shift,
		Reduce,
		Accept,
		SRConflict,
		RRConflict,
	}

	public class Action
	{
		public ActionType action;
		public Rule reduceByRule;
	}

	public class Symbol
	{
		public string lexeme;
		public bool isTerminal;
		public HashSet<Symbol> firstSet;
		public HashSet<Symbol> followSet;
		public int id;

		public override string ToString()
		{
			return lexeme;
		}
	}

	public class ParserGen
	{
		public static List<Symbol> symbols = new List<Symbol>();
		public static List<Rule> rules = new List<Rule>();
		public static List<State> states = new List<State>();
		private static List<State> dirtyStates = new List<State>();
		public static List<List<int>> gotoTable = new List<List<int>>();
		public static List<List<Action>> actionTable = new List<List<Action>>();
		public static Symbol Eof;
		public static Symbol Eps;
		public static bool mergeStates;

		private static HashSet<Symbol> FirstSetFrom(List<Symbol> str)
		{
			HashSet<Symbol> set = new HashSet<Symbol>();

			if (str.Count == 0 || str[0] == Eps)
				set.Add(Eps);
			else if (str[0].isTerminal)
				set.Add(str[0]);
			else if (!str[0].firstSet.Contains(Eps))
				set.UnionWith(str[0].firstSet);
			else
				set.UnionWith(str[0].firstSet.Where(x => x != Eps).Union(FirstSetFrom(str.GetRange(1, str.Count - 1))));

			return set;
		}

		private static void BuildFirstSets()
		{
			foreach (Rule rule in rules)
			{
				if (rule.rule.Count > 0 && rule.rule[0] != Eps)
				{
					if (rule.rule[0].isTerminal || rule.rule[0] == Eof)
						rule.result.firstSet.Add(rule.rule[0]);
				}
				else
					rule.result.firstSet.Add(Eps);
			}

			bool working = true;

			while (working)
			{
				working = false;
				foreach (Rule rule in rules)
				{
					int tmp = rule.result.firstSet.Count;
					rule.result.firstSet.UnionWith(FirstSetFrom(rule.rule));
					working |= tmp != rule.result.firstSet.Count;
				}
			}
		}

		private static void BuildFollowSets()
		{
			rules[0].result.followSet.Add(Eof);

			foreach (var symbol in symbols)
			{
				foreach (Rule rule in rules)
				{
					List<int> result = Enumerable.Range(0, rule.rule.Count).Where(i => rule.rule[i] == symbol).ToList();

					foreach (int index in result)
						if (index + 1 < rule.rule.Count && rule.rule[index + 1].isTerminal)
							symbol.followSet.Add(rule.rule[index + 1]);
				}
			}

			bool working = true;

			while (working)
			{
				working = false;

				foreach (Rule rule in rules)
				{
					for (int index = 0; index < rule.rule.Count; index++)
					{
						var symbol = rule.rule[index];

						int tmp = symbol.followSet.Count;

						if (index + 1 < rule.rule.Count)
						{
							var first = FirstSetFrom(rule.rule.Where((x, i) => i > index).ToList());
							symbol.followSet.UnionWith(first.Where(x => x != Eps));

							if (first.Contains(Eps))
								symbol.followSet.UnionWith(rule.result.followSet);
						}
						else
							symbol.followSet.UnionWith(rule.result.followSet);

						working |= tmp != symbol.followSet.Count;
					}
				}
			}
		}

		public static void EnsureTableCapacity(int requestedCapacity)
		{
			while (gotoTable[0].Count < requestedCapacity)
			{
				for (int j = 0; j < gotoTable.Count; j++)
					gotoTable[j].Add(-1);

				for (int j = 0; j < actionTable.Count; j++)
					actionTable[j].Add(new Action());
			}
		}

		private static void ExpandState(State state)
		{
			foreach (var nextSymbol in state.nextSymbols)
			{
				var nextItems = state.items.Where(x => x.dot < x.rule.rule.Count && x.rule.rule[x.dot] == nextSymbol).ToList();

				for (var i = 0; i < nextItems.Count; i++)
				{
					nextItems[i] = nextItems[i].Clone();
					nextItems[i].dot++;
				}

				State newState = CreateState(nextItems, states.Count);
				bool added = false;

				for (int i = 0; i < states.Count; i++)
				{
					var otherState = states[i];

					if (otherState.Equals(newState))
					{
						added = true;
						EnsureTableCapacity(state.id + 1);
						gotoTable[nextSymbol.id][state.id] = i;
					}
					else if (mergeStates && otherState.Similar(newState))
					{
						var merged = otherState.Merge(newState);
						if (merged.Equals(otherState))
						{
							added = true;
							EnsureTableCapacity(state.id + 1);
							gotoTable[nextSymbol.id][state.id] = i;
							break;
						}

						if (!merged.HasRRConflict())
						{
							states[i] = merged;
							dirtyStates.Add(merged);

							added = true;
							EnsureTableCapacity(state.id + 1);
							gotoTable[nextSymbol.id][state.id] = i;
							break;
						}
					}
				}

				if (!added)
				{
					EnsureTableCapacity(state.id + 1);
					gotoTable[nextSymbol.id][state.id] = newState.id;

					states.Add(newState);
					dirtyStates.Add(newState);
				}

				if (nextSymbol.isTerminal && nextSymbol != Eof)
				{
					actionTable[nextSymbol.id][state.id] = new Action() { action = ActionType.Shift };
				}
			}
		}

		private static State CreateState(List<Item> kernelItems, int id = -1)
		{
			State state = new State();
			state.id = id;
			state.items = kernelItems;
			state.nextSymbols = new HashSet<Symbol>();

			List<Item> dirtyItems = new List<Item>(kernelItems);

			while (dirtyItems.Count > 0)
			{
				var item = dirtyItems[0];
				dirtyItems.RemoveAt(0);

				if (item.dot < item.rule.rule.Count)
				{
					var symbolAtDot = item.rule.rule[item.dot];
					state.nextSymbols.Add(symbolAtDot);

					if (!symbolAtDot.isTerminal)
					{
						var newItemLookahead = FirstSetFrom(item.rule.rule.GetRange(item.dot + 1, item.rule.rule.Count - item.dot - 1));

						if (newItemLookahead.Count == 0 || newItemLookahead.Contains(Eps))
							newItemLookahead = new HashSet<Symbol>(newItemLookahead.Where(x => x != Eps).Union(item.lookahead));

						var rulesWithSymbolAsResult = rules.Where(x => x.result == symbolAtDot);

						foreach (var rule in rulesWithSymbolAsResult)
						{
							var newItem = new Item(rule, 0, new HashSet<Symbol>(newItemLookahead));

							var similarItems = state.items.Where(x => x.Similar(newItem));

							if (similarItems.Count() == 0)
							{
								state.items.Add(newItem);

								if (!dirtyItems.Contains(newItem))
									dirtyItems.Add(newItem);
							}
							else
							{
								foreach (var similarItem in similarItems)
								{
									if (!newItemLookahead.IsSubsetOf(similarItem.lookahead))
									{
										similarItem.lookahead.UnionWith(newItemLookahead);

										if (!similarItem.lookahead.IsSubsetOf(similarItem.rule.result.followSet))
										{
											//WTF!!!
											int a = 5;
										}

										if (!similarItem.lookahead.IsSubsetOf(similarItem.rule.rule.Last().followSet))
										{
											//WTF!!!
											int a = 5;
										}
										
										if (!dirtyItems.Contains(similarItem))
											dirtyItems.Add(similarItem);
									}
								}
							}
						}
					}
				}
			}

			return state;
		}

		private static void CreateTables()
		{
			foreach (var terminal in symbols.Where(x => x.isTerminal))
			{
				gotoTable.Add(new List<int>());
				actionTable.Add(new List<Action>());
			}

			foreach (var nonterminal in symbols.Where(x => !x.isTerminal))
				gotoTable.Add(new List<int>());
		}

		public static void Start(bool mergeStates)
		{
			Form1.writeEvent += Write;

			ParserGen.mergeStates = mergeStates;

			BuildFirstSets();
			BuildFollowSets();
			states.Clear();
			dirtyStates.Clear();
			actionTable.Clear();
			gotoTable.Clear();

			CreateTables();

			states.Add(CreateState(new List<Item> {new Item(rules[0], 0, new HashSet<Symbol>() { Eof })}, 0));
			dirtyStates.Add(states[0]);
			EnsureTableCapacity(1);

			while (dirtyStates.Count != 0)
			{
				var state = dirtyStates[0];
				dirtyStates.RemoveAt(0);

				ExpandState(state);
			}

			EnsureTableCapacity(states.Count);

			List<string> errors = new List<string>();
			
			foreach (var state in states)
			{
				var reducingItems = state.GetReducingItems();

				foreach (var item in reducingItems)
				{
					foreach (var symbol in item.lookahead)
					{
						var prevAction = actionTable[symbol.id][state.id].action;

						if (prevAction == ActionType.Error)
						{
							if (item.rule == rules[0])
								actionTable[symbol.id][state.id] = new Action() { action = ActionType.Accept };
							else
								actionTable[symbol.id][state.id] = new Action() { action = ActionType.Reduce, reduceByRule = item.rule };
						}
						else if (prevAction == ActionType.Reduce || prevAction == ActionType.RRConflict)
						{
							errors.Add("RR conflict in DFA state " + state.id + " for symbol " + symbol);
							actionTable[symbol.id][state.id] = new Action() { action = ActionType.RRConflict };
						}
						else if (prevAction == ActionType.Shift || prevAction == ActionType.SRConflict)
						{
							errors.Add("SR conflict in DFA state " + state.id + " for symbol " + symbol);
							actionTable[symbol.id][state.id] = new Action() { action = ActionType.SRConflict };
						}
					}
				}
			}

			if (errors.Count > 0)
			{
				new Thread(() => MessageBox.Show((IWin32Window)null, errors.GetRange(0, Math.Min(errors.Count, 20)).Aggregate("", (r, x) => r + "\n" + x))).Start();
				//MessageBox.Show((IWin32Window)null, errors.GetRange(0, Math.Min(errors.Count, 20)).Aggregate("", (r, x) => r + "\n" + x));
			}
		}

		public static void Write()
		{
			StringBuilder header = new StringBuilder();

			var terminals = ParserGen.symbols.Where(x => x.isTerminal).ToList();
			var nonterminals = ParserGen.symbols.Where(x => !x.isTerminal).ToList();

			header.Append("\t");
			header.Append(nonterminals[0]);
			header.Append(" = ");
			header.Append(terminals.Count);
			header.Append(",\n");

			for (int i = 1; i < nonterminals.Count; i++)
			{
				header.Append("\t");
				header.Append(nonterminals[i]);
				header.Append(",\n");
			}


			var wtf = new StreamWriter("Generated.h");
			wtf.Write(header.ToString());
			wtf.Close();

			StringBuilder code = new StringBuilder();

			code.Append("vector<string> Terminals = {");

			for (int i = 0; i < terminals.Count; i++)
			{
				code.Append("\"");
				code.Append(terminals[i]);
				code.Append("\", ");
			}

			code.Remove(code.Length - 2, 2);
			code.Append("};\n");

			code.Append("vector<string> Nonterminals = {");

			for (int i = 0; i < nonterminals.Count; i++)
			{
				code.Append("\"");
				code.Append(nonterminals[i]);
				code.Append("\", ");
			}

			code.Remove(code.Length - 2, 2);
			code.Append("};\n");

			code.Append("vector<Rule> Rules = {");

			for (int i = 0; i < ParserGen.rules.Count; i++)
			{
				code.Append("{");
				code.Append(ParserGen.rules[i].result.id);
				code.Append(", ");
				code.Append(ParserGen.rules[i].rule.Count);
				code.Append("}, ");
			}

			code.Remove(code.Length - 2, 2);
			code.Append("};\n");

			code.Append("vector<vector<Action>> actionTable = {");

			for (int i = 0; i < ParserGen.actionTable.Count; i++)
			{
				code.Append("{");

				for (int j = 0; j < ParserGen.actionTable[i].Count; j++)
				{
					code.Append("{");

					code.Append("ActionType::");
					code.Append(ParserGen.actionTable[i][j].action);
					code.Append(", ");
					if (ParserGen.actionTable[i][j].reduceByRule == null)
						code.Append("-1");
					else
						code.Append(ParserGen.actionTable[i][j].reduceByRule.id);

					code.Append("}, ");
				}

				code.Remove(code.Length - 2, 2);
				code.Append("}, ");
			}

			code.Remove(code.Length - 2, 2);
			code.Append("};\n");

			code.Append("vector<vector<int>> gotoTable = {");

			for (int i = 0; i < ParserGen.gotoTable.Count; i++)
			{
				code.Append("{");

				for (int j = 0; j < ParserGen.gotoTable[i].Count; j++)
				{
					code.Append(ParserGen.gotoTable[i][j]);
					code.Append(", ");
				}

				code.Remove(code.Length - 2, 2);
				code.Append("}, ");
			}

			code.Remove(code.Length - 2, 2);
			code.Append("};\n");

			wtf = new StreamWriter("Generated.cpp");
			wtf.Write(code.ToString());
			wtf.Close();
		}
	}
}