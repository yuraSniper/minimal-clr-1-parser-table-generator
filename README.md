# ABOUT #

This is my canonical LR(1) parser generator used for developing and debugging a grammar for [newLang](https://bitbucket.org/yuraSniper/newLang) programming language and compiler for it.
It is made using C# and WinForms.
