﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Minimal_LR1_parser_generator
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		public delegate void StartDelegate();
		public static event StartDelegate writeEvent;

		private void button1_Click(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();

			dialog.ShowDialog(this);
			if (string.IsNullOrEmpty(dialog.FileName))
				return;
			
			StreamReader stream = new StreamReader(dialog.OpenFile());
			string content = (stream.ReadToEnd());
			stream.Close();

			string[] rows = content.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

			string[] tmp = rows[0].Split(' ');

			ParserGen.symbols.Clear();
			ParserGen.states.Clear();
			ParserGen.rules.Clear();
			ParserGen.actionTable.Clear();
			ParserGen.gotoTable.Clear();

			for (int i = 1; i < tmp.Length; i++)
			{
				ParserGen.symbols.Add(new Symbol() { id = ParserGen.symbols.Count, lexeme = tmp[i], isTerminal = true, followSet = new HashSet<Symbol>() });
			}

			ParserGen.Eof = ParserGen.symbols.Last();
			ParserGen.Eps = ParserGen.symbols[ParserGen.symbols.Count - 2];

			for (int i = 1; i < rows.Length; i++)
			{
				string[] split = rows[i].Split(new string[] { "->" }, StringSplitOptions.RemoveEmptyEntries);

				if (split.Length > 0)
				{
					string lexeme = split[0].Trim('\t', ' ');

					if (!ParserGen.symbols.Exists(x => x.lexeme == lexeme))
					{
						ParserGen.symbols.Add(new Symbol() { id = ParserGen.symbols.Count, lexeme = lexeme, isTerminal = false, firstSet = new HashSet<Symbol>(), followSet = new HashSet<Symbol>() });
					}

					Rule rule = new Rule();
					rule.id = ParserGen.rules.Count;
					rule.rule = new List<Symbol>();
					rule.result = ParserGen.symbols.Find(x => x.lexeme == lexeme);

					ParserGen.rules.Add(rule);
				}
			}

			for (int i = 1; i < rows.Length; i++)
			{
				string[] split = rows[i].Split(new string[] { "->" }, StringSplitOptions.RemoveEmptyEntries);

				if (split.Length <= 0)
					continue;
				
				Rule rule = ParserGen.rules[i - 1];

				if (split.Length <= 1)
					continue;
				
				if (split[1].Trim().Length == 0)
					continue;

				string[] ruleStr = split[1].Trim(' ').Split(' ');

				foreach (var symbol in ruleStr)
				{
					int index = ParserGen.symbols.FindIndex(x => x.lexeme == symbol);
					if (index == -1)
					{
						if (symbol == "ε")
						{
							//rule.rule.Add(ParserGen.Eps);
							continue;
						}

						MessageBox.Show($"Symbol \"{symbol}\" isn\'t defined by grammar!");
						return;
					}

					rule.rule.Add(ParserGen.symbols[index]);
				}
			}

			ParserGen.Start(checkBox1.Checked);
			numericUpDown1.Maximum = ParserGen.states.Count - 1;
			numericUpDown1_ValueChanged(null, null);
		}

		private void numericUpDown1_ValueChanged(object sender, EventArgs e)
		{
			//if (ParserGen.states.Count > (int)numericUpDown1.Value)
				listBox1.DataSource = ParserGen.states[(int)numericUpDown1.Value].ToString().ToList();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			writeEvent();
			MessageBox.Show("CLR(1) parser tables generated successfully");
		}

		private void button4_Click(object sender, EventArgs e)
		{
			new Form2().Show();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			var currentState = ParserGen.states[(int)numericUpDown1.Value];
			if (currentState.items[0].dot != 0)
			{
				var prevSymbol = currentState.items[0].rule.rule[currentState.items[0].dot - 1];

				var state = ParserGen.gotoTable[prevSymbol.id].FindIndex(x => x == currentState.id);

				if (state != -1)
				{
					numericUpDown1.Value = state;
					numericUpDown1_ValueChanged(null, null);
				}
				else
					MessageBox.Show("Can't find ancestor for this state");
			}
		}
	}
}
